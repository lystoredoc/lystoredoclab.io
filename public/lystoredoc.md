## Documentation

1. [Se Rendre sur LyStore](#serendresurlystore)
2. [Qui peut accéder à LyStore ?](#quipeutaccderlystore)
3. [Comment déléguer l'accès à LyStore ?](#commentdlguerlaccslystore)
4. [Rejoindre la communauté ViaEduc](#rejoindrelacommunautviaeduc)
### Se rendre sur LyStore
Pour les personnes habilitées, LyStore est accessible depuis le réseau social éducatif MonLycée.net à l'adresse [https://ent.iledefrance.fr/lystore](https://ent.iledefrance.fr/lystore)

Vous pouvez également utiliser le système d'applications de MonLycée.net en ajoutant LyStore dans vos applications favorites :

![Accès LyStore](images/lystore_monlycee_access.png)

### Qui peut accéder à LyStore ?
LyStore est par défaut réservé aux profils de direction et de gestionnaires. Néanmoins, un système de délégation permet d'ajouter les utilisateurs souhaités.

### Comment déléguer l'accès à LyStore ?
Les administrateurs locaux (adml) de chaque établissement sont en mesure de réaliser cette opération de délégation.
Pour cela, il suffit de se rendre en tant qu'adml sur la console d'administration de son environnement de travail à l'adresse [https://ent.iledefrance.fr/directory/admin-console](https://ent.iledefrance.fr/admin) et comme le montre la capture suivante, de se rendre dans l'onglet **"Opérations sur les groupes manuels"** (étape 1).

![Accès LyStore](images/admin-console-view.png)

- étape 2 : Sélectionnez alors votre établissement scolaire dans le menu à gauche
- étape 3 : Sélectionnez le groupe manuel **LyStore** qui apparait alors
- étape 4 : Ajoutez un utilisateur dans ce groupe en cliquant simplement dessus

***(attention : un simple clic sur un utilisateur déjà dans le groupe le supprimera)***

### Rejoindre la communauté ViaEduc

Pour échanger, questionner, proposer de l'aide et faire évoluer la solution, rejoignez la communauté **LyStore** sur ViaEduc [https://www.viaeduc.fr/group/17974](https://www.viaeduc.fr/group/17974)

***Cette communauté n'a pas pour vocation d'être une plateforme de report de bugs.***
