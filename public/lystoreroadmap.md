***Vous trouverez ici toutes les nouveautés sur LyStore***

## Version du 13 mars 2018
- Mise en production de LyStore version 0.1.4
- Correctif : Les quantités d'articles n'étaient pas décomptées de la cagnotte.
- Correctif : Les options non obligatoires n'étaient pas prises en compte dans le prix affiché du panier
- Ergonomie : La date de livraison prévue n'est plus affichée car actuellement invérifiable

## Version du 5 mars 2018
- Mise en production de LyStore version 0.1.2
- Ouverture de la campagne PPE 2018
- Mise à disposition de la Documentation : Documentation, présentation, Roadmap, Campagnes
- Système de délégation : Les adml peuvent donner accès au LyStore de l'établissement via la console d'administration
