## Campagnes

LyStore concerne l'ensemble des campagnes d'équipements proposées par la Région île-de-France, à savoir :
- Campagne du Plan Prévisionnel d'équipements numériques
- Campagne de Mesures de rentrée
- Campagne de Premiers Équipements
- Campagne de Compléments et de Renouvellement
- Campagne de Mission de Lutte contre le Décrochage scolaire

### Gestion du Plan Prévisionnel d'équipements
Cette campagne est ouverte toute l'année. Elle concerne les équipements numériques tels que les ordinateurs, les imprimantes, les vidéoprojecteurs, les visualiseurs. Pour l'année 2018, elle débute le 5 mars 2018. Elle fonctionne sur un système de panier/commande et de **cagnotte** allouée pour chaque établissement éligible. Les demandes formulées par les lycées sont traitées toutes les 6 semaines environ.

### Autres Campagnes
Les autres campagnes d'équipements seront accessibles sur LyStore courant novembre 2018 selon un calendrier qui sera communiqué ultérieurement.
