## À propos

> LyStore est une place de marché open source proposée aux lycées franciliens par le Conseil Régional d'île-de-France. Elle permet à ces derniers, à partir d'un catalogue mis à disposition par la Région, de passer des commandes d'équipements numériques (Ordinateurs en format tour, ordinateurs portables, vidéoprojecteurs et imprimantes) en ligne. À l'aide d'un système de panier, les lycées seront désormais en mesure de suivre l'évolution de leur commande et d'être notifié lorsque leurs demandes sont prises en compte.

### Génèse du projet
En 2016, la Région île-de-France lance le Plan Prévisionnel d'Équipements pour tous les lycées franciliens. À partir d'une enveloppe allouée en début d'année civile, chaque lycée doit pouvoir commander des équipements informatiques disponibles sur les marchés à bons de commande de la Région. Suite à la commande de développement d'un nouvel outil en octobre 2017, la Région est fière de vous présenter la nouvelle place de marché des lycées franciliens, LyStore, disponible depuis le 5 mars 2018.

### Simplicité
Simple, ergonomique, intuitif, accessible depuis le réseau social éducatif MonLycée.net, il donne accès à plusieurs dizaines de références pour que les lycées d'île-de-France puissent maintenir leurs parcs informatiques en toute autonomie.

### Agilité & Open source
Dans le cadre de ce projet, la Région île-de-France s'inscrit pleinement dans une démarche open source. Avec l'aide de nos prestataires CGI et ODE, LyStore a été développé en un temps record en utilisant des cycles itératifs. Le code source est disponible en version GPL v3 sur la forge du réseau social éducatif MonLycée.net :

[Code source de LyStore](https://github.com/OPEN-ENT-NG/lystore)
